## ---------  WHAT THIS SCRIPT DOES ---------- ##
## It splits an ICS file with multiple events into 
## a directory of invidivdual ICS events as individual files.
##  
## Purpose: can be handled by syncevolution on Ubuntu Touch thus making 
## the multiple smaller files useable in Ubuntu Touch Calendar.

from sys import argv
import re
import os
from datetime import datetime
try:
    from tkinter.filedialog import askopenfilename
except ImportError:
    print("Sorry, Import Error on tkinter module (doesn't exist).")
import readline

## Opens the single source ICS file which contains the events.
## 
## TODO Probably need to make it so that it searches the directory for any 
## ICS file (.ics) since we might not know the defined name.
## For now, ARGV so that user can just simply input the ics file 
## name into the terminal and run it.

## Two options to get the source file are included. 
## Manual input is the default and first.

## ====== Source file input option 1 - Enter path by user input with script====== ##

source_ics = argv[1]

## ====== Source file input 2 - Manual User Input with `tkinter` module ====== ##
## Note: does not currently work on Ubuntu Touch (not in packages)
#source_ics = askopenfilename()
#print(source_ics) 

with open(source_ics, 'r') as f:
    ics_content = f.read()

splitStuff = ics_content.split('\n')

## This function creates a new directory with a
## unique timestamp name each time the script runs

def create_events_dir():
    cwd = os.getcwd()
    dt_object = datetime.now()
    nice_date = datetime.strftime(dt_object, "%Y_%m_%d_%M_%S")
    events_path = str('{}'.format(cwd) + '/{}'.format(nice_date))
    print(events_path)
    create_events_dir.path = events_path
    os.mkdir(events_path)

create_events_dir()


## This pulls the attribute of the events_path in the function
## above and makes it available globally.  TODO (Perhaps this
## isn't the right way to do this and return function should be
## used?)

event_write_path = create_events_dir.path

## START OF THE FUNCTION THAT LOOPS THROUGH AND SPLITS OUT CONTENT ##

def split_events():
    
    start_str = 'BEGIN:VEVENT'
    end_str = 'END:VEVENT'
    event_content_lines = []
    counter = 0

    for x in splitStuff:

        if x == start_str:

            start_event_block = x            
            event_content_lines = []
            counter += 1
            print('BEGIN:VCALENDAR')

        elif x == end_str:

            close_event_block = x
            
            print("========= PRINTING EVENT =========")
            print("This block will appear in filename: " + '{}.ics'.format(counter))
            print("===========================")
            ## To delete - Following prints - for debugging 
            print('BEGIN:VCALENDAR')
            print(start_event_block)
            print(* event_content_lines, sep = '\n')
            print(close_event_block)
            print('END:VCALENDAR')

            ## Function to write the split events to own file
            ## In correct directory (new dir created in function
            ## create_events_dir above)
            def write_events():
                os.chdir(event_write_path)
                with open('{}.ics'.format(counter), 'w') as f:
                    f.write('BEGIN:VCALENDAR' + "\n")
                    f.write(start_event_block + "\n")
                    ## Loops through and converts list into strings on own line
                    for item in event_content_lines:
                        f.write(item + "\n")
                    f.write(close_event_block)
                    f.write("\n")
                    f.write('END:VCALENDAR')
                os.chdir('..')
            write_events()

        else:
            ## To delete - for debugging
            # print("=========ELSE BLOCK=========")
            event_content_lines.append(x)
         
split_events()