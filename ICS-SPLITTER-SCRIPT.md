# ics Splitter

## Objective

Write a simple script to split an ics file into multiple ics files, one file per event.
An event should be everything from `BEGIN:VEVENT` to `END:VEVENT`.

## Background

`basic.ics` is a snippit of an ical file from my todo list app. I want to be able to import this into
Ubuntu Touch's calendar, but the syncevolution command seems to only support one
event per ics file. In the end I want to write a cron job on my phone that pulls
the ics, runs this script to split it into multiple files, then import that into
the UT calendar via syncevolution.


What I think I need to do:

1. open the ics file
2. iterate through it to find the `BEGIN:VEVENT` to `END:VEVENT`.
3. split the contents of what's between `BEGIN:VEVENT` to `END:VEVENT`.
4. write those contents of the step above into a new file with a unique file name, a name which has logic and can be used by the chron job? do these new files that are created need to exist in a new dir? can python make a new dir?
yes. do this using OS module to make a dir...

5. repeat 2-4 untnil there are no more `BEGIN:VEVENT` to `END:VEVENT`. in the file
6. exit

