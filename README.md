# ICS SPLITTER

## What this script does - General

It splits an original ICS file that contains multiple events into a directory of invidivdual ICS events as individual files. 

Original Purpose: can be handled by syncevolution on Ubuntu Touch thus making the multiple smaller files useable in Ubuntu Touch Calendar.

## What this script does - Detailed

It creates a new directory in the same path as the python script based on a local time stamp.  This helps to make sure the name of directory doesn't repeat when the script runs and content files overwritten if you run it twice in the same day.

It scans through the source ICS file and finds the `BEGIN:VEVENT` line and the `END:VEVENT` and prints those two and everything in between them including a counter to the terminal.

Finally, it saves the content of each of those new mini-ics events to new individual files into the newly-created directory with an incrementing file name of "1.ics", "2.ics" etc (matches counter).

## How to use it

### First Way (Default) - With Terminal Only

1. Open a terminal
2. Navigate inside the directory with the script
3. Enter the Python command (Python must be installed) with the script name (`ics-splitter.py`) with the path to the source ics file. 

Here is an example of what the command might look like. ICS file path, would be adjusted to match user's file location:

`python3.6 ics-splitter.py ~/Documents/basic.ics`

### Second Way - Optional

If you use this option be sure to comment-out this line before running:
`source_ics = argv[1]`

1. Open script file
2. Uncomment these lines by removing the '#':
`#source_ics = askopenfilename()`
`#print(source_ics)`
3. Run the script in the terminal as follows:
`python3.6 ics-splitter.py`

Now when script runs it will prompt you for a source ICS file with a GUI

